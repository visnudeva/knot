**KNOT**

A memo/note app focused on simplicity, minimalism and speed of use, without option, distraction free, no ads, no tracker, FOSS.
Only one note possible with a clear and a save button, a dark and light mode following the system theme. 
Very simple so every idea can be saved easily without loosing the thought.


About the concept and name:

An antiquated form of memory aid. The basic idea is that you tie a knot in the corner of your handkerchief the night before you have to perform some task. When you go to blow your nose or wipe your face or perform any of the other actions associated with a handkerchief you will see the knot and remember your duty.

Problems arise, however, when it comes to recalling precisely what it is you are supposed to remember. Also, with the rise of the paper tissue, and the decline of the handkerchief, this practice has fallen out of use. Nowadays it is far easier to use a memo app on your mobile phone.

https://liberapay.com/visnudeva
